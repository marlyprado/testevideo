class EmployeePayment
	def initialize(dead = false, separated = false, retired = false)
		@dead = dead
		@separated = separated
		@retired = retired
	end

	def pay_amount
		return dead_amount if @dead
		return separated_amount if @separated
		return retired_amount if @retired
		normal_pay_amount
	end

	def dead_amount
		0
	end

	def separated_amount
		500
	end

	def retired_amount
		750
	end

	def normal_pay_amount
		1000
	end
end