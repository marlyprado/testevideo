class BeforeExample
	def initialize(dead = 0, separated = 0, retired = 0)
		@dead = dead
		@separated = separated
		@retired = retired
	end
	
	def pay_amount
		if @dead
			result = dead_amount
		else
			if @separated
				result = separated_amount
			else
				if @retired
					result = retired_amount
				else
					result = normal_pay_amount
				end
			end
		end
		result
	end

	def dead_amount
		0
	end

	def separated_amount
		500
	end

	def retired_amount
		750
	end

	def normal_pay_amount
		1000
	end
end