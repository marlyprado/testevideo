require "employee_payment"
describe EmployeePayment do
	context 'What is the pay amount value' do
		it 'when he is dead' do
			dead = true
			expect(EmployeePayment.new(dead).pay_amount).to eq 0
		end
		it 'when he is separated' do
			dead = false
			separated = true
			expect(EmployeePayment.new(dead, separated).pay_amount).to eq 500
		end
		it 'when he is retired' do
			dead = false
			separated = false
			retired = true
			expect(EmployeePayment.new(dead, separated, retired).pay_amount).to eq 750
		end
		it 'when he is normal' do
			expect(EmployeePayment.new().pay_amount).to eq 1000
		end
	end
end